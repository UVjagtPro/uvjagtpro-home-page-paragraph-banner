<?php

	/**
	* Plugin Name: UVjagtPro - Home page paragraph banner
	* Description: This plugin displays the content of the home page 
	* Author: Kim Nyegaard Andreasen
	* Version: 1.0
	*/	

	/*
	* Inspiration
	* - https://www.sitepoint.com/adding-custom-meta-boxes-to-wordpress/ 
	* - https://www.taniarascia.com/wordpress-part-three-custom-fields-and-metaboxes/comment-page-2/#comments
	*/	

	include "custom-fields.php";

	function home_page_paragraph_banner() 
	{
		$frontpage_id = get_option( 'page_on_front' );

		$the_query = new WP_Query( 'page_id='. $frontpage_id ); 

		while ($the_query -> have_posts()) : $the_query -> the_post();  

			$meta = get_post_meta( get_the_ID(), 'your_fields', true ); ?>
			
			<div>
               <div class="front-page-content-banner">
					<p><?php echo $meta['focus'];?></p>
				</div>

			</div>


	     <?php endwhile;
	} 

	add_action( 'home_between_header_and_body', 'home_page_paragraph_banner' );

?>