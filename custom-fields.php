<?php

	function text_in_focus_meta_box()
	{
	    add_meta_box(
	    	'text-in-focus', 
	    	'Tekst i fokus', 
	    	'text_in_focus_markup', 
	    	'page', 
	    	'normal', 
	    	'high', 
	    	null
    	);
	}

	add_action("add_meta_boxes", "text_in_focus_meta_box");	

	function text_in_focus_markup()
	{
		global $post;  
	
		$meta = get_post_meta( $post->ID, 'your_fields', true ); ?>

		<input type="hidden" name="your_meta_box_nonce" value="<?php echo wp_create_nonce( basename(__FILE__) ); ?>">

    	<!-- All fields will go here -->

		<p>
			<textarea name="your_fields[focus]" id="your_fields[focus]" rows="5" cols="30" style="width:100%;"><?php echo $meta['focus']; ?></textarea>
		</p>

	<?php }

	/*######################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	############################### Save meta box data to database #########################################
	########################################################################################################
	########################################################################################################
	########################################################################################################
	########################################################################################################*/

	function save_fields_meta( $post_id ) 
	{   

		// verify nonce
		if ( !wp_verify_nonce( $_POST['your_meta_box_nonce'], basename(__FILE__) ) ) 
		{
			return $post_id; 
		}
		
		// check autosave
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
		
		// check permissions
		if ( 'page' === $_POST['post_type'] ) 
		{
			if ( !current_user_can( 'edit_page', $post_id ) ) 
			{
				
				return $post_id;

			} elseif ( !current_user_can( 'edit_post', $post_id ) ) {
				
				return $post_id;

			}  
		}
		
		$old = get_post_meta( $post_id, 'your_fields', true );
		$new = $_POST['your_fields'];

		if ( $new && $new !== $old ) 
		{
		
			update_post_meta( $post_id, 'your_fields', $new );
		
		} elseif ( '' === $new && $old ) {
			
			delete_post_meta( $post_id, 'your_fields', $old );
		
		}
	}
	
	add_action( 'save_post', 'save_fields_meta' );

?>